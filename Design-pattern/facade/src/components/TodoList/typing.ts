import { ITodoData } from "../../typings";

export interface IInputOptions {
  wrapperEl: HTMLElement,
  placeholder: string,
  buttonText: string
}

export interface IListOptions {
  wrapperEl: HTMLElement,
  todoData: ITodoData[],
}

export { ITodoData };
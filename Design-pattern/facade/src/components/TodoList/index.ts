import { ITodoData } from '../../typings';
import Input from './Subs/Input';
import List from './Subs/List';
import { IInputOptions, IListOptions } from './typing';

class TodoList {
  private el: HTMLElement;
  private todoData: ITodoData[];
  private input: Input;
  private list: List;
  private todoWrapper: HTMLElement;

  constructor(el: HTMLElement, todoData: ITodoData[]) {
    this.el = el;
    this.todoData = todoData;
    this.todoWrapper = document.createElement('div');
  }

  public init() {
    this.createComponents();
    this.render();
    this.bindEvent();
  }

  private createComponents() { // 实例化组件
    this.input = new Input(<IInputOptions>{
      wrapperEl: this.todoWrapper,
      placeholder: '请输入',
      buttonText: '增加'
    });
    this.list = new List(<IListOptions>{
      wrapperEl: this.todoWrapper,
      todoData: this.todoData
    });
  }

  private render() { // 渲染到页面
    this.input.render();
    this.list.render();
    this.el.appendChild(this.todoWrapper);
  }

  private bindEvent() { // 绑定事件处理函数
    this.input.bindEvent();
    this.list.bindEvent();
  }
}

export default TodoList;
import ModalFactory from './factory.js';

;((doc) => {

  const oModal = doc.getElementsByClassName('modal')[0];
  const oBtnGroup = doc.getElementsByClassName('btn-group')[0];
  const modalFactory = new ModalFactory(oModal);

  const init = () => {
    bindEvent();
  }

  function bindEvent() {
    oBtnGroup.addEventListener('click', handleBtnClick, false);
  }

  function handleBtnClick(e) {
    const tar = e.target;
    const tagName = tar.tagName.toLowerCase();

    if(tagName === 'button') {
      const status = tar.dataset.status;
      // changeStatus(status); // 这种方式太内聚 不能扩展

      const modal = modalFactory.create('这是一个工厂模式的应用场景', status);

      switch (status) {
        case 'S':
          modal.navToURL('http://www.baidu.com');
          break;
        case 'W':
          modal.outputInfo('这是一个警告提示');
          break;
        case 'E':
          modal.outputInfo('这是一个错误提示');
          break;
        default:
          break;
      }
    }
  }

  // function changeStatus(status) {
  //   switch (status) {
  //     case 'S':
  //       oModal.className = 'modal success';
  //       break;
  //     case 'W':
  //       oModal.className = 'modal warning';
  //       break;
  //     case 'E':
  //       oModal.className = 'modal error';
  //       break;
    
  //     default:
  //       break;
  //   }
  // }

  init();
})(document);
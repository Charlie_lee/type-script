import TodoList from "./TodoList";

;((doc) => {

  const oTodo: HTMLElement = document.querySelector('.todo'),
        oInput: HTMLInputElement = oTodo.querySelector('input'),
        oAddBtn: HTMLElement = oTodo.querySelector('.add-btn'),
        oTodoList: HTMLElement = oTodo.querySelector('.todo-list');

  const todoList = TodoList.create(oTodoList);

  const init = (): void => {
    bindEvent();
  }

  function bindEvent() {
    oAddBtn.addEventListener('click', handleAddBtnClick, false);
    oTodoList.addEventListener('click', handleListClick, false);
  }

  function handleAddBtnClick() {
    const val: string = oInput.value.trim();

    if (!val.length) return;

    todoList.addItem({
      id: Date.now(),
      content: val,
      completed: false
    })

    oInput.value = '';
  }

  function handleListClick(e: MouseEvent) {
    const tar = e.target as HTMLElement;
    const tagName = tar.tagName.toLowerCase();

    if (tagName === 'input' || tagName === 'button') {
      const id: number = parseInt(tar.dataset.id);

      switch (tagName) {
        case 'input':
          todoList.toggleCompleted(id);
          break;
        case 'button':
          todoList.removeItem(id);
          break;
        default:
          break;
      }
    }
  }

  init();

})(document);
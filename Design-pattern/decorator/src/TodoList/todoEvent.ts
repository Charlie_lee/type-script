import { ITodo } from ".";

let todoData: ITodo[] = [];

export function addTodo() { // 如果装饰器传了参数需要返回新的函数
  return function ( // 这个返回的函数就是装饰器函数
    target: any, // 当前装饰的函数的容器 -> TodoList.prototype 当到原型上
    methodName: string, // 被装饰的函数的名称
    descriptor: PropertyDescriptor // 当前方法的属性(描述符)
  ) {
    // console.log(target, methodName, descriptor);
    const _originFn = descriptor.value; // 保存原来的函数

    descriptor.value = function (todo: ITodo | null) {

      const _todo: ITodo | null = todoData.find((t: ITodo) => t.content === todo.content)

      if (_todo) {
        alert('该项已存在');
        return
      }

      todoData.push(todo);
      _originFn.call(this, todo); // 重新执行原来的函数 需要重新指向原实例
    }
  }
}

export function removeTodo(
  target: any, // 一般设置为any
  methodName: string,
  descriptor: PropertyDescriptor
) {
  const _originFn = descriptor.value;

  descriptor.value = function (id: number) {
    todoData = todoData.filter((todo: ITodo) => todo.id !== id);
    _originFn.call(this, id);
  }
}


export function changeCompletedTodo(
  target: any,
  methodName: string,
  descriptor: PropertyDescriptor
) {
  const _originFn = descriptor.value;

  descriptor.value = function (id: number) {
    todoData = todoData.map((todo: ITodo) => {
      if(todo.id === id) {
        todo.completed = !todo.completed;

        _originFn.call(this, id, todo.completed);
      }
      return todo;
    })
  }
}

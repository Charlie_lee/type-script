
# 组件 -> commit -> mutation -> state -> 视图变更 -> 响应式 

  - 事件驱动： 开发过程中，每一次更改都是通过一个类型(事件类型)去更改

  - Vuex (工作流)
    1. 组件 -> dispatch(派发) -> action
    2. dispatch -> type(actionType，通过这个type来确定) -> 某个action
    3. action -> commit(调用) -> mutation (方法)
    4. mutation -> change(变更) -> state(数据、状态)
    5. render方案: state -> 数据流 -> 视图

  - 文件集合
    1. actionTypes      action类型
    2. actions          调用mutation的方法
    3. mutations        更改state的方法
    4. state            中央数据管理容器
    
    5. store出口        actions、mutations、state统一到仓库里进行管理


# 组件划分
  - TodoList
    1. TodoInput -> 输入的组件
    2. TodoList -> 列表组件
      - TodoItem -> 列表项
        1. 完成或未完成的选择   checkbox
        2. 删除功能            button
        3. 正在做的确认功能     button








# ts-vue3-todo

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).




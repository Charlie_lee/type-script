// 专门负责模板 渲染的 或一些模板方法

import { ITodoData } from "./typings";


class TodoTemplate {


  protected todoView (todo: ITodoData): string {
    const { id, content, completed } = todo;

    return `
      <input type="checkbox" ${ completed ? 'checked': '' } data-id="${ id }" />
      <span style="text-decoration: ${ completed ? 'line-through': 'none' }">${content}</span>
      <button data-id="${ id }">删除</button>
      `;
  }
}

export default TodoTemplate;
export interface ITodoData {
  // 对对象数据来说一般用 interface或是type 来声明
  // 用interface时 一般变量前面加大写 I
  
  id: number;
  content: string;
  completed: boolean;
}


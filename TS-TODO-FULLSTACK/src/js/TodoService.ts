// 专门用来发送请求

import $ from 'jquery';
import { ITodoData } from './typings';

export function getTodoList( // 装饰器模式 一般都是这三个参数
  target: any,
  methodName: string,
  descriptor: PropertyDescriptor
): void {
  // console.log(target, methodName, descriptor)
  // 首先保存原有的 init 方法
  const _origin = descriptor.value;
  // console.log('_origin的this', this) // undefined
  // 再重写init方法 获取数据 并传值
  descriptor.value = function(todoData: ITodoData[]) {
    $.get('http://localhost:8080/todolist').then(res => {
      if (!res) return
      if (res.statusCode === 200) {
        todoData = JSON.parse(res.data)
      }
    }).then(() => {
      // console.log('todoData', todoData)
      // console.log('this', this)
      // _origin.call(todoData)
      // 因为_origin为重写的方法，this指向全局，严格模式下为undefined 找不到todoEvent
      // 而descriptor.value 是init方法 this指向的还是内部作用域
      // this要指向当前作用域下的this
      _origin.call(this, todoData)
    })
  }

}

export function addTodo(
  target: any,
  methodName: string,
  descriptor: PropertyDescriptor
): void {
  const _origin = descriptor.value;

  descriptor.value = function (todo: ITodoData) {
    $.post('http://localhost:8080/add', { todo: JSON.stringify(todo) }).then(res => {
      if (!res) return
      if (res.statusCode === 100) {
        alert('该项已存在！')
        return
      }
      _origin.call(this, todo)
    })
  }
}

export function removeTodo(
  target: any,
  methodName: string,
  descriptor: PropertyDescriptor
): void {
  const _origin = descriptor.value;

  descriptor.value = function (target: HTMLElement, id: number) {
    $.post('http://localhost:8080/remove', { id }).then(res => {
      if (!res) return
      _origin.call(this, target, id)
    })
  }
}

export function toggleTodo(
  target: any,
  methodName: string,
  descriptor: PropertyDescriptor
): void {
  const _origin = descriptor.value;

  descriptor.value = function (target: HTMLElement, id: number) {
    $.post('http://localhost:8080/toggle', { id }).then(res => {
      if (!res) return
      _origin.call(this, target, id)
    })
  }
}